#include <stdint.h>
#include <stddef.h>

extern "C" {
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "common.h"
#include "large.h"
}

unsigned short USPS_MSB_Math_CRC11GenerateFrameCheckSequence(unsigned char *ByteArrayPtr);

// extern "C" int FUZZ_INIT_WITH_ARGS(int *argc, char ***argv) {
extern "C" int FUZZ_INIT() {

  // Add global setup code here - called once before fuzzing starts

  return 0;
}

extern "C" int FUZZ(const uint8_t *Data, size_t Size) {

  // process fuzzer input (*Data) and setup local objects necessary to call the function under test

  USPS_MSB_Math_CRC11GenerateFrameCheckSequence(/* add arguments here */);

  // reset state and free all locally allocated resources

  return 0;
}
