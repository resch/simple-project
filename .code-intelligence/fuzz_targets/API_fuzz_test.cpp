#include <stddef.h>
#include <stdint.h>

// extern "C" int FUZZ_INIT_WITH_ARGS(int *argc, char ***argv) {
extern "C" int FUZZ_INIT() {

  // Add global setup code here - called once before fuzzing starts

  return 0; // Non-zero return values are reserved for future use.
}

extern "C" int FUZZ(const uint8_t *Data, size_t Size) {

  // process fuzzer input (*Data) and setup local objects necessary to call the function under test

  CallYourAPI(Data, Size); // TODO call your API here

  // reset state and free all locally allocated resources

  return 0; // Non-zero return values are reserved for future use.
}
