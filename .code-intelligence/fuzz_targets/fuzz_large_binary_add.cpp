#include <stdint.h>
#include <stddef.h>

extern "C" {
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "large.h"
}

void binary_add(short *accumulator, short *input_buffer);

// extern "C" int FUZZ_INIT_WITH_ARGS(int *argc, char ***argv) {
extern "C" int FUZZ_INIT() {

  // Add global setup code here - called once before fuzzing starts

  return 0;
}

extern "C" int FUZZ(const uint8_t *Data, size_t Size) {

  // process fuzzer input (*Data) and setup local objects necessary to call the function under test

  binary_add(/* add arguments here */);

  // reset state and free all locally allocated resources

  return 0;
}
